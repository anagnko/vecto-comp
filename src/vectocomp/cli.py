# Copyright 2023 Joint Research Center, European Commission
#
# Licensed under the EUPL, Version 1.2 or later as soon they
# will be approved by the European Commission - subsequent
# versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the
# Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl5
#
# Unless required by applicable law or agreed to in
# writing, software distributed under the Licence is
# distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
# express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
"""
CLI entry point with **Typer** lib.
"""
import json
import logging
import re
import typing as t
from pathlib import Path

import typer
from jinja2 import TemplateNotFound
from rich import print
from rich.columns import Columns
from rich.console import Group
from rich.markdown import Markdown
from rich.panel import Panel
from rich.syntax import Syntax
from rich.tree import Tree
from typing_extensions import Annotated

from . import DEBUG, __summary__, __version__
from .xspitter import (
    extract_template_details,
    is_wildcard,
    list_templates,
    render_template_xml,
)

log = logging.getLogger(__name__)
init_log_level = logging.getLevelName(logging.getLogger().level)

app = typer.Typer(
    # pretty_exceptions_enable=False,  # Disable beauty in case of troubles.
    no_args_is_help=True,
    help=__summary__,
    context_settings={"help_option_names": ["-h", "--help"]},
)


def setup_logging(verbose: int):
    level = logging.getLogger().level
    for i in range(verbose):
        level = level - 10
    logging.basicConfig(level=level)
    log.info(f"Logging level: {logging.getLevelName(level)}")


_generic_panel = "Generic Options"
_verbose_option = typer.Option(
    "--verbose",
    "-v",
    count=True,
    help=f"More verbosity than {init_log_level};  "
    "levels: FATAL, ERROR, WARNING, INFO, DEBUG, NOTSET",
    rich_help_panel=_generic_panel,
)
_quiet_option = typer.Option(
    "--quiet",
    "-q",
    count=True,
    help=f"Less verbosity than {init_log_level};  "
    "levels: FATAL, ERROR, WARNING, INFO, DEBUG, NOTSET",
    rich_help_panel=_generic_panel,
)
_debug_option = typer.Option(
    "--debug",
    is_flag=True,
    help=f"⚙ Enable uncommon checks and balances.",
    rich_help_panel=_generic_panel,
)


@app.callback(invoke_without_command=True)
def root_options(
    verbose: Annotated[int, _verbose_option] = 0,
    quiet: Annotated[int, _quiet_option] = 0,
    debug: Annotated[bool, _debug_option] = False,
    version: Annotated[
        t.Optional[bool],
        typer.Option(
            "--version",
            "-V",
            is_eager=True,
            help="Show program version",
        ),
    ] = None,
):
    if version:
        print(__version__)
        raise typer.Exit()

    DEBUG = debug
    setup_logging(verbose - quiet)


def _autocomplete_templates(incomplete: str):
    # Dont search by extension, and only autocomplete if not wildcarded already.
    wildcard = incomplete if is_wildcard(incomplete) else f"{incomplete}*"
    return list_templates(wildcard, with_summaries=True)


_component_argument = typer.Argument(
    metavar="COMPONENT.EXT",
    help="Component template to use (issue `ls` cmd to list them).",
    autocompletion=_autocomplete_templates,
)


_vars_panel = "Variable values"

_value_parsers = {
    "": lambda x: x,
    "+": int,
    "%": float,
    # "?": utils.str2bool,
    ":": json.loads,
    "@": eval,
    #'@': ast.literal_eval ## best-effort security: http://stackoverflow.com/questions/3513292/python-make-eval-safe
}


def _parse_key_op_value(
    data: str,
) -> t.Tuple[str, str]:
    """
    Set value for template variable(s).

    :return: (var, value)

    * In VARPATH=VALUE pairs, values result in strings.  For other types,
    substitute '=' operator with:.
        +=     : int
        %=     : float
        ?=     : boolean
        :=     : parsed as YAML/JSON
        @=     : load file/URL
    * Boolean string-values are case insensitive:
        False  : (n)o  | (f)alse | off | 0
        True   : (y)es | (t)rue  | on  | 1
        None   : (d)efault/<empty>
    * All other string-values are case-sensitive.

    #>>> _parse_key_op_value("key")
    Traceback (most recent call last):
    click.exceptions.BadParameter:

    >>> _parse_key_op_value("VARPATH=VALUE")
    ('VAR', '', 'VALUE')

    >>> _parse_key_op_value("a/b=VALUE")
    ('a/b', '', 'VALUE')

    >>> _parse_key_op_value("/a/b=VALUE")
    ('/a/b', '', 'VALUE')

    >>> _parse_key_op_value("a[2]/b=VALUE")
    ('a[2]/b', '', 'VALUE')

    >>> _parse_key_op_value("/a/b[0]=VALUE")
    ('/a/b[0]', '', 'VALUE')

    >>> _parse_key_op_value("a[2]/=VALUE")
    Traceback (most recent call last):
    click.exceptions.BadParameter: Expected a variable assignment like `VARPATH=VALUE`, got: a[2].=VALUE
    ...
    """
    try:
        var, value = data.split("=", 1)
    except ValueError:
        raise typer.BadParameter(
            f"Not a `VARPATH=VALUE` syntax!",
            param=data,
            param_hint="--set",
        )
    varop, value = var.strip(), value.strip()
    m = re.match(r"^(/?[_A-Za-z][\w/\.]*)\s*([+%?:@]?)$", varop)
    if not m:
        raise typer.BadParameter(
            f"Expected a XPATH expression as variable, got: {varop}",
            param=data,
            param_hint="--data",
        )

    varpath, op = m.groups()
    return (varpath, _value_parsers[op](value))


_default_out = "derrived from COMPONENT.EXT"


def _unknown_template_msg(template):
    return (
        f"Unknown template(s) {template!r}, "
        f"available templates: {', '.join(list_templates())}"
    )


@app.command(no_args_is_help=True)
def create(
    component: Annotated[str, _component_argument],
    out: Annotated[
        typer.FileTextWrite,
        typer.Argument(
            help="Filename to create; `-` for STDOUT, `+` for clipboard.",
        ),
    ] = _default_out,
    data: Annotated[
        t.Optional[t.List[str]],
        typer.Option(
            "--data",
            "-d",
            help="Define template variable value(s) (or read them from file).",
            metavar="VARPATH=VALUE",
            rich_help_panel=_vars_panel,
            parser=_parse_key_op_value,
            show_default=False,
        ),
    ] = None,
    datapath: Annotated[
        t.Optional[str],
        typer.Option(
            help="The XPATH in input data to start reading variable values from.",
            metavar="VARPATH",
            rich_help_panel=_vars_panel,
            show_default=False,
        ),
    ] = "/",
):
    """
    Generate component file from some template.
    """
    user_vars = dict(data)
    try:
        good_vars = extract_template_details(component).vars
    except TemplateNotFound:
        raise typer.BadParameter(
            _unknown_template_msg(component),
            param=component,
            param_hint="COMPONENT.EXT",
        )
    stray_vars = user_vars.keys() - good_vars
    if stray_vars:
        raise typer.BadParameter(
            f"Template `{component}` doesn't use x{len(stray_vars)} of the given variable(s):"
            " {', '.join(stray_vars)}",
            param=data,
            param_hint="--set",
        )

    txt = render_template_xml(component, user_vars)

    # LAZY File or created nevertheless the rename.
    if out.name == "+":
        from pandas.io.clipboard import clipboard_set

        clipboard_set(txt)
        log.info("Created in clipboard.")
    else:
        if out.name == _default_out:
            out.name = Path(component)
        out.write(txt)
        log.info("Created: %s", out.name)


def template_details_tree(component):
    details = extract_template_details(component, with_comments=True)
    tree = Tree(f"[b magenta]Template [bold]`{component}`:")
    tree.add(Group("📁 path:", f"[bold]{details.fpath}"))
    varTree = tree.add(Group("🏷 variables:"))
    for var, msg in sorted(details.vars.items()):
        var_items = [f"[bold]{var}[/]"]
        if msg:
            var_items.append(Markdown(f"💡 {msg}"))
        varTree.add(Group(*var_items))
    tree.add(
        Group(
            "📄 [bold]contents:",
            Panel(
                Syntax(
                    details.content,
                    lexer=Syntax.guess_lexer(details.fpath, details.content),
                    theme="default",
                    line_numbers=True,
                )
            ),
        )
    )

    return tree


@app.command(no_args_is_help=True)
def show(components: Annotated[t.List[str], _component_argument]):
    """
    Examine the details for component template(s).
    """
    unknown = []
    for i, tpl in enumerate(components):
        try:
            print(template_details_tree(tpl))
        except TemplateNotFound:
            unknown.append(f"{i}: {tpl}")

    if unknown:
        raise typer.BadParameter(
            _unknown_template_msg(unknown), param=components, param_hint="COMPONENT.EXT"
        )


@app.command()
def ls(
    pattern: Annotated[
        t.Optional[t.List[str]],
        typer.Argument(
            help="Template file-extensions or wildcard-patterns to search (eg. `xml`, `*`, `*json`).",
        ),
    ] = None,
):
    """
    List component templates known to this app.
    """
    print(Columns(list_templates(*pattern)))


def main():
    app()
