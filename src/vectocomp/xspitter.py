# Copyright 2023 Joint Research Center, European Commission
#
# Licensed under the EUPL, Version 1.2 or later as soon they
# will be approved by the European Commission - subsequent
# versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the
# Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl5
#
# Unless required by applicable law or agreed to in
# writing, software distributed under the Licence is
# distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
# express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
"""
Generate XML files for Vecto components.
"""
import functools as fnt
import logging
import re
import typing as t
from datetime import datetime
from fnmatch import fnmatch
from pathlib import Path

import jinja2 as j2
import jinja2.compiler as j2compiler
import jinja2.exceptions as j2ex
import pandas as pd
import typer

from . import __title__

log = logging.getLogger(__name__)
app_dir = Path(typer.get_app_dir(__title__))


def _required_filter(value, errmsg=None, *, none_allowed=None, non_empty=None):
    """
    Demand value is defined, and (optionally) not None or empty

    :param errmsg:
        if given, included in the exception raised.
    :raise:
        j2.UndefinedError, j2ex.FilterArgumentError
    """
    if j2.is_undefined(value) or (not none_allowed and value is None):
        if value is None:
            raise j2ex.FilterArgumentError(errmsg or f"a 'required' is None")
        value["scream"] # will raise `UndefinedError` with var name included.  

    if non_empty and not isinstance(value, bool) and not bool(value):
        if not errmsg:
            errmsg = f"Must not be empty, was: {value!r}"
        raise j2ex.FilterArgumentError(errmsg)
    return value


def _xml_datetime_filter(value, fmt="%YYYY-%m-%dT%H:%M:%S.%f%Z") -> str:
    """A jinja-filter to parse+format datetime to XML format for Vecto."""
    date = pd.to_datetime(value)
    return date.strftime(fmt)


def _iterrow_filter(matrix, with_index: t.Optional[bool] = None) -> list:
    """
    Try iterating over rows of a dataframe, dict items or list items.

    :param with_index:
        if truthy, include dataframe-index as the 1st element of iterated row.
    :return:
        an iterator over dataframe tuples, dict-items or `matrix` itself.
        Iterates empty if `matrix` undefined.
    :raise:
        Screams if `matrix` is None or string.

    > NOTE: If dataframe column-names are invalid Python identifiers, repeated, or
    > start with an underscore, `DataFrame.itertuples()` renames them to positional names.
    """
    if isinstance(matrix, str):
        raise j2ex.FilterArgumentError(f"Refusing to iterate string-value: {matrix}")
    if j2.is_undefined(matrix):
        return ()
    try:
        return matrix.itertuples(index=with_index)
    except AttributeError:
        try:
            return matrix.items()
        except AttributeError:
            return iter(matrix)


def _column_filter(row, *indices: t.Union[int, str]):
    """
    A jinja-filter trying conjecutive dict or pandas.Series indices (int, str, etc).

    Attribute access is tried if an index starts dot(``.``).
    """
    for i in indices:
        try:
            if isinstance(i, str) and i.startswith("."):
                return getattr(row, i[1:])
            return row[i]
        except (KeyError, IndexError, AttributeError, TypeError):
            log.debug("Failed indexing %r from: %r", i, row)
    return None


@fnt.lru_cache()
def jinja_env():
    user_tpl_dir = app_dir / "templates"
    log.info("Searching template overrides in user dir: %s", user_tpl_dir)
    loader = j2.ChoiceLoader(
        (
            j2.PackageLoader("vectocomp"),
            j2.FileSystemLoader(user_tpl_dir),
        )
    )
    jenv = j2.Environment(loader=loader, autoescape=j2.select_autoescape())
    jenv.globals["now"] = datetime.utcnow
    jenv.filters["required"] = _required_filter
    jenv.filters["xmldate"] = _xml_datetime_filter
    jenv.filters["rows"] = _iterrow_filter
    jenv.filters["column"] = _column_filter

    return jenv


_fnpattern_chars = set("*?[")


def is_wildcard(s: str) -> bool:
    return any(_fnpattern_chars & set(s))


@fnt.lru_cache()
def list_templates(*exts_or_patterns: str, with_summaries: bool = None):
    if exts_or_patterns:
        are_wildcards = {p: is_wildcard(p) for p in exts_or_patterns}
        filters = [
            fnt.partial(fnmatch, pat=(p if contains_arterisk else f"*{p}"))
            for p, contains_arterisk in are_wildcards.items()
        ]

        def filter_func(template):
            return any(1 for f in filters if f(template))

        args = {"filter_func": filter_func}
    else:
        args = {}

    tlist = jinja_env().list_templates(**args)

    if with_summaries:
        jenv = jinja_env()

        def template_summary(tpl) -> t.Optional[str]:
            tpl_src, _fpath, _ = jenv.loader.get_source(jenv, tpl)
            comments = extract_template_comments(tpl_src)
            return comments.pop(None, None)

        tlist = [(tpl, template_summary(tpl)) for tpl in tlist]

    return tlist


def render_template_xml(template: str, values: dict, *, strict_undefined=None):
    jenv = jinja_env()
    tpl = jenv.get_template(template)

    return tpl.render(values, undefined=strict_undefined)


class TemplateDetails(t.NamedTuple):
    fpath: str
    content: str
    vars: t.Dict[str, t.Optional[str]]
    summary: t.Optional[str]


@fnt.lru_cache()
def extract_template_comments(src: str) -> t.Dict[t.Optional[str], str]:
    """
    Extract jinja2 comments like ``{# summary: <msg> #}`` or ``{# var(<varname>): <msg>) #}``.

    >>> extract_template_comments("{# summary: <msg> #} {# var(varname): <msg>) #}")
    {None: '<msg>', 'varname': '<msg>)'}

    >>> extract_template_comments(\"""
    ...    without summary {# var(varname): a var-help message
    ...    that spans multiple lines) #} and still works!
    ... \""")
    {'varname': 'a var-help message\\n   that spans multiple lines)'}

    >>> extract_template_comments("No jinja comments")
    {}
    >>> extract_template_comments("{# irrelevant comments #}")
    {}
    """

    def match_groups(m: t.Match) -> t.Tuple[str, str]:
        if m:
            # x2 groups matched when ``var(<varname>): <help-msg>)``
            # x1 group matched when ``summary: <help-msg>``
            *var, msg = m.groups()
            var = var[0] if var else None
            if var or msg:
                return (var, msg)

    pairs = [
        pair
        for m in re.finditer(
            r"(?is)\{#-?\s*(?:summary|var\((\w+)\)):\s*(.+?)\s*-?#\}", src
        )
        if (pair := match_groups(m))
    ]

    return dict(pairs)


class TrackingCodeGenerator(j2compiler.CodeGenerator):
    def __init__(self, environment):
        super().__init__(environment, "<introspection>", "<introspection>")
        self.undeclared_identifiers = {}  # type: ignore

    def write(self, x: str) -> None:
        """Don't write."""

    def enter_frame(self, frame):
        super().enter_frame(frame)

        for _, (action, param) in frame.symbols.loads.items():
            if action == "resolve" and param not in self.environment.globals:
                self.undeclared_identifiers[param] = None


def find_undeclared_variables(ast) -> t.Dict[str, t.Optional[str]]:
    """
    Adapted from :class:`jinja2.meta.find_undeclared_variables()` to preserve order.
    """
    codegen = TrackingCodeGenerator(ast.environment)
    codegen.visit(ast)
    return codegen.undeclared_identifiers


def extract_template_details(
    template: str, with_comments: bool = None
) -> TemplateDetails:
    jenv = jinja_env()
    tpl_src, tpl_fpath, _ = jenv.loader.get_source(jenv, template)
    parsed_content = jenv.parse(tpl_src)
    vars = find_undeclared_variables(parsed_content)

    summary = None
    if with_comments:
        comments = extract_template_comments(tpl_src)
        summary = comments.pop(None, None)
        vars.update((k, v) for k, v in comments.items() if k in vars)

    return TemplateDetails(tpl_fpath, tpl_src, vars, summary)
