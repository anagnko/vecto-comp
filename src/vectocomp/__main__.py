# Copyright 2023 Joint Research Center, European Commission
#
# Licensed under the EUPL, Version 1.2 or later as soon they
# will be approved by the European Commission - subsequent
# versions of the EUPL (the "Licence");
# You may not use this work except in compliance with the
# Licence.
# You may obtain a copy of the Licence at:
#
# https://joinup.ec.europa.eu/software/page/eupl5
#
# Unless required by applicable law or agreed to in
# writing, software distributed under the Licence is
# distributed on an "AS IS" basis,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
# express or implied.
# See the Licence for the specific language governing
# permissions and limitations under the Licence.
"""
CLI entry point whether `pip`-installed (packaged or editable mode) or not.

```shell
$ vectocomp
$ python -m vectocomp
$ python <path/to/folder/vectocomp>
```
"""
__package__ = "vectocomp"
from . import cli

if __name__ == "__main__":
    cli.main()
