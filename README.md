# vecto-comp: file processor

![pypi-version](https://img.shields.io/pypi/v/codecomp?label=PyPi%20release&logo=pypi)
![python-ver](https://img.shields.io/pypi/pyversions/codecomp?label=Python&logo=pypi)
![dev-status](https://img.shields.io/pypi/status/codecomp)
![codestyle](https://img.shields.io/badge/code%20style-black-black)
![License](https://img.shields.io/pypi/l/codecomp)

A console app to work with Vecto *component* files eg. generate XML files.

## --(scrapped;  parts to be used with [copier](https://copier.readthedocs.io/))--

[Vecto](https://code.europa.eu/groups/vecto/-/wikis/home) is the simulation tool
that has been developed by the European Commission and is certify the CO2 emissions
and Fuel Consumption from Heavy Duty Vehicles (trucks, buses and coaches)
since Jan 2019.

Currently this tool generates XML files for components.


## Usage

Install (prefferably in a *venv*) and launch it like this:

```shell
$ pip install vectocomp
$ vectocomp --help
$ vectocomp --install-completion
```

...

## Template authoring

- *VSCode:* Install **Better Jinja** extension:

  - launch *VS Code Quick Open* `(Ctrl+P)`, 
    paste the following command, and press enter:

    ```
    ext install samuelcolvin.jinjahtml
      ```

  - ensure the *language mode* is set to `Jinja <whatever>` at the bottom right corner 
    of the window when editing templates 
    (right-click on it, or `(Ctrl+P)` and start typing `Change Language Mode`).

- Create new templates in user config dir;  it's rougly  ``~/.config/vectocomp/templates`` 
  on *Linux*, ``C:/Users/<user>/AppData/Local/vectocomp`` on *Windows*.

- You may use these filters:
- 
  - `required` - ...
  - `rows` - ...
  - `column` - ...
  - `xmldate` - ...
  - `now` - ...
 
- Document templates and their variables like that:
 
  ```xml+jinja
  {# summary: explain *purpose* & *usage* (at least) of the template. #}
  <?xml version="1.0" encoding="utf-8"?>
  <!-- cml code here  ->
  
  {# var(some_var): **markdown** documentation for `some_var`, dumped with `show` command #}

  <!-- more cml code here  ->
  ...
  
  ```
