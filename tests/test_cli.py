import logging

import pytest
from typer.testing import CliRunner

from vectocomp import cli


@pytest.mark.parametrize(
    "name, cmd, doc",
    [
        ("app", cli.app, cli.__doc__),
        ("create", None, None),
        ("show", None, None),
        ("ls", None, None),
    ],
)
def test_cmd_help_from_doc(name, cmd, doc):
    runner = CliRunner()
    if cmd:  # root command
        assert doc
        args = ["-h"]
    else:
        assert not doc
        args = [name, "-h"]
        cmd = getattr(cli, name)
        doc = cmd.__doc__
    result = runner.invoke(cli.app, args)
    assert result.exit_code == 0
    assert doc.strip()[0] in result.stdout


def test_create_polite_on_unknown_template():
    runner = CliRunner(mix_stderr=False)
    result = runner.invoke(cli.app, ["create", "missing", "-"])
    assert result.exit_code == 2
    assert "available templates: " in result.stderr


def test_show_polite_on_unknown_template(caplog):
    caplog.set_level(logging.ERROR)
    runner = CliRunner(mix_stderr=False)
    result = runner.invoke(cli.app, ["show", "missing", "-"])
    assert result.exit_code == 1
    assert "available templates: " in caplog.messages[0]
