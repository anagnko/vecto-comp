import logging
import re

import jinja2 as j2
import jinja2.exceptions as j2ex
import pandas as pd
import pytest

from vectocomp import xspitter


@pytest.mark.parametrize(
    "inp, exp",
    [
        ([], ...),
        ({}, []),
        ((), []),
        ("", []),
        (None, []),
        ([[1, 2, 3], ["4", "5", "6"]], ...),
        ("abc", list("abc")),
        ({1: 1, "b": 2, "o": 3}, [(1, 1), ("b", 2), ("o", 3)]),
        ([{1: 1, "b": 2}, [1, "o", 3.14]], ...),
        (pd.DataFrame([[1, 2, 3], ["4", "5", "6"]]), [[1, 2, 3], ["4", "5", "6"]]),
    ],
)
def test_iterrow_filter(inp, exp):
    if exp is ...:
        exp = inp
    for row_inp, row_exp in zip(xspitter._iterrow_filter(inp), exp, strict=True):
        for i, j in zip(row_inp, row_exp, strict=True):
            assert i == j


@pytest.mark.parametrize(
    "src, err",
    [
        ("{{ b }}", ""),
        ("{{ b | required }}", j2.UndefinedError("'b' is undefined")),
        ("{{ a | required }}", j2ex.FilterArgumentError("a 'required' is None")),
        (
            "{{ b | required(non_empty=true) }}",
            j2.UndefinedError("'b' is undefined"),
        ),
        (
            "{{ a | required(non_empty=true) }}",
            j2ex.FilterArgumentError("a 'required' is None"),
        ),
        (
            "{{ b | required(none_allowed=true) }}",
            j2.UndefinedError("'b' is undefined"),
        ),
        ("{{ a | required(none_allowed=true) }}", "None"),
        ("{{ c | required() }}", "()"),
        (
            "{{ c | required(non_empty=true) }}",
            j2ex.FilterArgumentError("empty, was: ()"),
        ),
        ("{{ c | required(none_allowed=true) }}", "()"),
        (
            "{{ c | required(non_empty=true, none_allowed=true) }}",
            j2ex.FilterArgumentError("empty, was: ()"),
        ),
    ],
)
def test_j2_filter_required(src, err):
    jenv = xspitter.jinja_env()
    tpl = jenv.from_string(src)
    values = {"a": None, "c": ()}
    if isinstance(err, Exception):
        with pytest.raises(type(err), match=str(err)):
            tpl.render(values)
    else:
        res = tpl.render(values)
        if err is not None:
            assert err in res


@pytest.mark.parametrize(
    "matrix",
    [
        [[1, 2, 3], ["4", "5", "6"]],
        [{"torque": 2, 0: 1, "loss": 3}, ["4", "5", "6"]],
    ],
)
def test_render_axle(matrix, caplog):
    caplog.set_level(logging.DEBUG)
    treedata = {
        "ratio": 3.14,
        "matrix": matrix,
    }
    if matrix is ...:
        del treedata["matrix"]

    res = xspitter.render_template_xml("axle.xml", treedata)
    inverse = not bool(matrix) or matrix is ...
    assert inverse ^ bool(
        re.search(r"1\.0.+2\.0.+3\.0.+4\.0.+5\.0.+6\.0", res, re.DOTALL)
    )
    logs = caplog.text
    assert inverse ^ bool(re.search(r"Failed indexing 'speed'", logs))
    assert inverse ^ bool(re.search(r"Failed indexing 'torque'", logs))
    assert inverse ^ bool(re.search(r"Failed indexing 'loss'", logs))


def test_render_axle_screams_with_null_matrix():
    with pytest.raises(j2.UndefinedError, match="'matrix' is undefined"):
        xspitter.render_template_xml("axle.xml", {})
