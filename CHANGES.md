# CHANGES

## TODOs

### Minimum Viable Product

- See also [TODOs](https://code.europa.eu/vecto/vecto-comp/wiki/TODO).
- Set interpolation variables with `-d` from varous sourcef:
  - string, float, int, yaml, from file (csv, json, excel(xleash)).
  - Accept xpath+xleash as variable nanes.
  - Parse further read-kwargs with *pyparse*.
- Autocomplete template variables.
- Prompt for missing values.
    - Live layout with template & render preview.
- Document templates (summary & vars) exposed in autocompletions & prompts.
- readme:
  - "usage" template section
  - "authoring" template section
  - find out how Typer includes terminal output in its docs.
  - describe features
- Release to PyPI.

### Good to have

- Config cmd to add/edit templates in user config dir.

## Releases

### 2+ Oct 2023: SCRAPPED

- Still buggy :-(
- ... then reminded of **cookiecutter**....and [found](https://www.cookiecutter.io/article-post/cookiecutter-alternatives) a 
  recent alternative [**copier**](https://copier.readthedocs.io/)
  from which i discovered [jinja2-ansible-filters](https://gitlab.com/dreamer-labs/libraries/jinja2-ansible-filters)
  (so `required` filter exists there as `mandatory`).

### 1 Oct 2023: v0.0.1-alpha: buggy!

- Enhance template authoring: matrix vars, `required` filter`.
- Still nested data not supported.
 
### 21-24 Sept 2023: v0.0.0-alpha

- Created git repo with minimal CLI based on `typer` & `jinja2`
- x3 cmds: `ls`, `show`, `create`
- `show` extracts template variables.
- Logging with `--verbose` & `--quiet` options.
- Shell autocompletes of template names (not yet & variables).
- Pytest established.